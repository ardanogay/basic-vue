export const ProductList =
    [{ id: 1, name: "Saat", price: 250, amount: 5 },
    { id: 2, name: "İphone", price: 5550, amount: 15 },
    { id: 3, name: "Bilgisayar", price: 1050, amount: 25 },
    { id: 4, name: "Ayakkabı", price: 450, amount: 35 },
    { id: 5, name: "Mont", price: 350, amount: 75 },
    { id: 6, name: "Şapka", price: 50, amount: 5 }]